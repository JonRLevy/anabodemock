﻿using AnabodeMock.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AnabodeMock.Services
{
    public interface IDataService
    {
        Task<List<CheckListItem>> GetChecklistItemsAsync();
        Task<Let> GetLetAsync();
    }
}
