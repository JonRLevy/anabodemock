﻿using AnabodeMock.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AnabodeMock.Services
{
    public class MockDataService : IDataService
    {
        private readonly List<CheckListItem> checkListItems = new List<CheckListItem>();
        private readonly Let let = new Let() { Address = "9a Waterloo Road", PostalCode = "SE7 4JY" };

        public MockDataService()
        {
            checkListItems.Add(new CheckListItem { Name = "Inventory Check-in", Checked = true });
            checkListItems.Add(new CheckListItem { Name = "Professional Cleaning", Checked = false });
            checkListItems.Add(new CheckListItem { Name = "Furniture (Regulations Compliant)", Checked = true });
            checkListItems.Add(new CheckListItem { Name = "Smoke Detectors", Checked = true });
            checkListItems.Add(new CheckListItem { Name = "Carbon Monoxide Alarm", Checked = false });
            checkListItems.Add(new CheckListItem { Name = "Energy Performance Certificate", Checked = false });
        }

        public Task<List<CheckListItem>> GetChecklistItemsAsync()
        {
            return Task.FromResult(checkListItems);
        }

        public Task<Let> GetLetAsync()
        {
            return Task.FromResult(let);
        }
    }
}
