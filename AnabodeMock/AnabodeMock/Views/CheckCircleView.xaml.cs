﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnabodeMock.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CheckCircleView : ContentView
	{
        private const float circleStrokeWidth = 6f;
        private static readonly SKPaint outerCircleStroke = new SKPaint()
        {
            Style = SKPaintStyle.Stroke,
            Color = Color.FromHex("2d2e36").ToSKColor(),
            StrokeWidth = circleStrokeWidth,
            IsAntialias = true
        };
        private static readonly SKPaint innerCircleFill = new SKPaint()
        {
            Style = SKPaintStyle.Fill,
            Color = Color.FromHex("8ec89a").ToSKColor(),
            IsAntialias = true
        };

        public static readonly BindableProperty IsCheckedProperty = BindableProperty.Create(nameof(IsChecked), typeof(bool), typeof(CheckCircleView), default(bool), propertyChanged: OnCheckedChanged);

        public bool IsChecked
        {
            get => (bool)GetValue(IsCheckedProperty);
            set => SetValue(IsCheckedProperty, value);
        }

        public CheckCircleView ()
		{
			InitializeComponent ();
		}

        private static void OnCheckedChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            CheckCircleView checkCircleView = bindable as CheckCircleView;
            checkCircleView?.circleCanvas.InvalidateSurface();
        }

        private void circleCanvas_PaintSurface(object sender, SkiaSharp.Views.Forms.SKPaintSurfaceEventArgs e)
        {
            SKImageInfo info = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;

            canvas.Clear();

            canvas.Translate(info.Width / 2f, info.Height / 2f);

            float outerRadius = Math.Min(info.Height, info.Width);

            canvas.Scale(outerRadius / 200f);

            canvas.DrawCircle(0f, 0f, 100f - circleStrokeWidth / 2, outerCircleStroke);
            if (IsChecked)
                canvas.DrawCircle(0f, 0f, 66f, innerCircleFill);
        }
    }
}