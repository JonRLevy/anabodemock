﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnabodeMock.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SVGTabStackLayout : StackLayout
	{
        public static readonly BindableProperty TabTitleProperty = BindableProperty.Create(nameof(TabTitle), typeof(string), typeof(SVGTabStackLayout), "");

        public string TabTitle
        {
            get => (string)GetValue(TabTitleProperty);
            set => SetValue(TabTitleProperty, value);
        }

        public static readonly BindableProperty SVGResourceIdProperty = BindableProperty.Create(nameof(SVGResourceId), typeof(string), typeof(SVGTabStackLayout), "");

        public string SVGResourceId
        {
            get => (string)GetValue(SVGResourceIdProperty);
            set => SetValue(SVGResourceIdProperty, value);
        }

        public static readonly BindableProperty IsTabSelectedProperty = BindableProperty.Create(nameof(IsTabSelected), typeof(bool), typeof(SVGTabStackLayout), false);
        public bool IsTabSelected
        {
            get => (bool)GetValue(IsTabSelectedProperty);
            set => SetValue(IsTabSelectedProperty, value);
        }

        public static readonly BindableProperty TabContentProperty = BindableProperty.Create(nameof(TabContent), typeof(View), typeof(SVGTabStackLayout), null);
        public View TabContent
        {
            get => (View)GetValue(TabContentProperty);
            set => SetValue(TabContentProperty, value);
        }

        public SVGTabStackLayout ()
		{
			InitializeComponent ();
		}
	}
}