﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnabodeMock.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SVGIcon : Frame
    {
        private readonly SKCanvasView contentCanvas = new SKCanvasView();

        public static readonly BindableProperty ResourceIdProperty = BindableProperty.Create(nameof(ResourceId), typeof(string), typeof(SVGIcon), default(string), propertyChanged: RedrawCanvas);

        public string ResourceId
        {
            get => (string)GetValue(ResourceIdProperty);
            set => SetValue(ResourceIdProperty, value);
        }

        public SVGIcon()
        {
            InitializeComponent();
            Content = contentCanvas;

            contentCanvas.PaintSurface += contentCanvas_OnPaintSurface;
        }

        private static void RedrawCanvas(BindableObject bindable, object oldvalue, object newvalue)
        {
            SVGIcon svgIcon = bindable as SVGIcon;
            svgIcon?.contentCanvas.InvalidateSurface();
        }

        private void contentCanvas_OnPaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {
            SKCanvas canvas = e.Surface.Canvas;
            canvas.Clear();

            if (string.IsNullOrEmpty(ResourceId))
                return;

            using (Stream stream = GetType().Assembly.GetManifestResourceStream(ResourceId))
            {
                SkiaSharp.Extended.Svg.SKSvg svg = new SkiaSharp.Extended.Svg.SKSvg();
                svg.Load(stream);

                SKImageInfo info = e.Info;
                canvas.Translate(info.Width / 2f, info.Height / 2f);

                SKRect bounds = svg.ViewBox;
                float widthRatio = info.Width / bounds.Width;
                float heightRatio = info.Height / bounds.Height;

                float ratio = Math.Min(widthRatio, heightRatio);

                canvas.Scale(ratio);
                canvas.Translate(-bounds.MidX, -bounds.MidY);

                canvas.DrawPicture(svg.Picture);
            }
        }
    }
}