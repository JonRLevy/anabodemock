﻿using AnabodeMock.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnabodeMock.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CheckListView : ContentView
	{
        CheckListViewModel viewModel => BindingContext as CheckListViewModel;

        public CheckListView ()
		{
			InitializeComponent ();
		}

        private void checkListListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            CheckListItemViewModel tappedCheckListItemViewModel = e.Item as CheckListItemViewModel;
            if (tappedCheckListItemViewModel != null)
            {
                tappedCheckListItemViewModel.SwitchCheckedCommand.Execute(null);
            }
        }

    }
}