﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnabodeMock.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MyLetPage : ContentPage
	{
		public MyLetPage ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
            topImage.Source = ImageSource.FromResource("AnabodeMock.Resources.topimage.jpg", GetType().Assembly);
        }
    }
}