﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnabodeMock.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SVGTabbedView : ContentView
	{
        public static readonly BindableProperty SVGTabsProperty = BindableProperty.Create(nameof(SVGTabs), typeof(SVGTabViewList), typeof(SVGTabbedView), null, propertyChanged: setupTabs);

        public SVGTabViewList SVGTabs
        {
            get => (SVGTabViewList)GetValue(SVGTabsProperty);
            set => SetValue(SVGTabsProperty, value);
        }

        public static readonly BindableProperty SelectedTabIndexProperty = BindableProperty.Create(nameof(SelectedTabIndex), typeof(int), typeof(SVGTabbedView), -1, propertyChanged: selectedTabIndexChanged);

        public int SelectedTabIndex
        {
            get => (int)GetValue(SelectedTabIndexProperty);
            set => SetValue(SelectedTabIndexProperty, value);
        }

        public SVGTabbedView ()
		{
            SVGTabs = new SVGTabViewList();
			InitializeComponent ();
		}

        static void setupTabs(BindableObject bindable, object oldvalue, object newvalue)
        {
            SVGTabbedView view = bindable as SVGTabbedView;
            if (view is null)
                return;

            if (view.tabGrid!=null)
            {
                foreach (View childView in view.tabGrid.Children)
                {
                    SVGTabStackLayout tab = childView as SVGTabStackLayout;
                    if (tab is null)
                        continue;

                    tab.GestureRecognizers.Clear();
                }
                view.tabGrid.Children?.Clear();
            }
            view.tabGrid?.ColumnDefinitions.Clear();
            view.contentGrid?.Children.Clear();

            SVGTabViewList tabs = newvalue as SVGTabViewList;

            if (view.SelectedTabIndex >= 0 && view.SelectedTabIndex < view.SVGTabs.Count)
            {
                view.SVGTabs[view.SelectedTabIndex].IsTabSelected = true;
                view.SVGTabs[view.SelectedTabIndex].IsVisible = true;
            }

            if (tabs is null)
                return;

            for (int idx = 0; idx < tabs.Count; idx++)
            {
                view.tabGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1.0, GridUnitType.Star) });
                view.tabGrid.Children.Add(tabs[idx], idx, 0);
                TapGestureRecognizer tgr = new TapGestureRecognizer();
                tgr.Tapped += (sender, e) =>
                {
                    SVGTabStackLayout tab = sender as SVGTabStackLayout;
                    if (tab is null)
                        return;

                    view.SelectedTabIndex = view.SVGTabs.IndexOf(tab);
                };
                tabs[idx].GestureRecognizers.Add(tgr);
                if (idx != view.SelectedTabIndex)
                    tabs[idx].TabContent.IsVisible = false;

                view.contentGrid.Children.Add(tabs[idx].TabContent);
            }
        }

        static void selectedTabIndexChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            SVGTabbedView view = bindable as SVGTabbedView;
            if (view is null)
                return;

            int oldIndex = (int)oldvalue;
            if (oldIndex >= 0 && oldIndex < view.SVGTabs.Count)
            {
                view.SVGTabs[oldIndex].IsTabSelected = false;
                view.SVGTabs[oldIndex].TabContent.IsVisible = false;
            }

            int newIndex = (int)newvalue;
            if (newIndex >= 0 && newIndex < view.SVGTabs.Count)
            {
                view.SVGTabs[newIndex].IsTabSelected = true;
                view.SVGTabs[newIndex].TabContent.IsVisible = true;
            }

        }
    }

    public class SVGTabViewList : List<SVGTabStackLayout>
    {

    }
}