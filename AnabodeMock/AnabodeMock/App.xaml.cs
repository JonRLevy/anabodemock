﻿using AnabodeMock.Services;
using AnabodeMock.ViewModels;
using AnabodeMock.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace AnabodeMock
{
    public partial class App : Application
    {
        public IDataService AppDataService { get; } = new MockDataService();
        public LetViewModel App_LetViewModel { get; } = new LetViewModel();  
        
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage (new MyLetPage() { BindingContext = App_LetViewModel });
        }

        protected async override void OnStart()
        {
            await App_LetViewModel.InitAsync();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
