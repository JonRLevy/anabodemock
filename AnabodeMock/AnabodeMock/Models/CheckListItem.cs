﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnabodeMock.Models
{
    public class CheckListItem
    {
        public string Name { get; set; }
        public bool Checked { get; set; }
    }
}
