﻿using AnabodeMock.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace AnabodeMock.ViewModels
{
    public class CheckListItemViewModel : BindableBase
    {
        private CheckListItem model;
        public CheckListItem Model
        {
            get => model;
            set => SetProperty(ref model, value);
        }

        public CheckListItemViewModel(CheckListItem model)
        {
            this.model = model;
        }

        public bool Checked => model.Checked;
        public string Name => model.Name;


        void switchChecked_Execute()
        {
            model.Checked = !model.Checked;
            OnPropertyChanged(nameof(Checked));
        }

        private Command switchCheckedCommand;
        public ICommand SwitchCheckedCommand => switchCheckedCommand ?? (switchCheckedCommand = new Command(switchChecked_Execute));
    }
}
