﻿using AnabodeMock.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnabodeMock.ViewModels
{
    public class CheckListViewModel : BindableBase
    {

        private bool isBusy;
        public bool IsBusy
        {
            get => isBusy;
            set => SetProperty(ref isBusy, value);
        }

        private List<CheckListItemViewModel> checkList;
        public List<CheckListItemViewModel> CheckList
        {
            get => checkList;
            set => SetProperty(ref checkList, value);
        }

        public Task InitAsync()
        {
            return ReadDataAsync();
        }
        
        public async Task ReadDataAsync()
        {
            IsBusy = true;

            List<CheckListItem> items = await((App)App.Current).AppDataService.GetChecklistItemsAsync();

            CheckList = items.Select(i => new CheckListItemViewModel(i)).ToList();

            IsBusy = false;
        }
    }
}
