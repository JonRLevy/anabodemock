﻿using AnabodeMock.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AnabodeMock.ViewModels
{
    public class LetViewModel : BindableBase
    {
        private bool isBusy;
        public bool IsBusy
        {
            get => isBusy;
            set => SetProperty(ref isBusy, value);
        }


        private Let model;
        public Let Model
        {
            get => model;
            set => SetProperty(ref model, value);
        }

        private CheckListViewModel checkListViewModel;
        public CheckListViewModel CheckListViewModel
        {
            get => checkListViewModel;
            set => SetProperty(ref checkListViewModel, value);
        }

        public Task InitAsync()
        {
            return ReadDataAsync();
        }

        public async Task ReadDataAsync()
        {
            IsBusy = true;

            Model = await ((App)App.Current).AppDataService.GetLetAsync();

            CheckListViewModel = new CheckListViewModel();
            await CheckListViewModel.InitAsync();

            IsBusy = false;
        }
    }
}
